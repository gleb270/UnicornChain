pragma solidity ^0.4.24;
contract Unicorn {
    
    //MVP product
    //Development:  Pozdnyakov Nikita   https://github.com/nikitosing/
    //              Shanshin Gleb       https://github.com/gleb270
    //GitHub:       UnicornChain        https://github.com/gleb270/UnicornChain
    
    mapping (address => uint) balances;
    mapping (address => bool) didReg;
    
    address owner;
    address ownerTeam = 0x88577FdeCF4dccD92202D6022723e5154AE1df93;
    address ASI = 0x1053B44E7d68aA41e6a53a6dD4F5d4d045A1598A;
    
    string public symbol;
    string public  name;
    uint8 public decimals;
    
    constructor() public {
        symbol = "UNC";
        name = "Unicorn";
        decimals = 18;
        owner = msg.sender;
        balances[msg.sender] = 1000000000;
    }
    
    event TeamReg(address[] _team);
    event Transfer(address _from, address _to, uint _amount);
    event PersonalReg(address _user);
    
    function () public payable {
        revert();
    }

    function balanceOf(address _user) public view returns (uint) {
        return balances[_user];
    }
    
    function transfer(address _to, uint _amount) public {
        require(balances[msg.sender] >= _amount);
        balances[_to] += _amount;
        balances[msg.sender] -= _amount;
        emit Transfer(msg.sender, _to, _amount);
    }
    
    function myBalance() public view returns (uint) {
        return balances[msg.sender];
    }

    function personalReg(address _user) public {
        require(msg.sender == owner);
        require(!didReg[_user]);
        balances[_user] += 1000000;
        balances[ownerTeam] += 100000;
        balances[ASI] += 10000;
        balances[owner] -= 1110000;
        didReg[_user] = true;
        emit PersonalReg(_user);
    }
    
    function teamReg(address _user1, address _user2, address _user3, address _user4, address _user5, address _user6) public {
        require(msg.sender == owner);
        address[] storage _users;
        _users.push(_user1);
        _users.push(_user2);
        _users.push(_user3);
        _users.push(_user4);
        _users.push(_user5);
        _users.push(_user6);
        for (uint i = 0; i < _users.length; i++){
            balances[_users[i]] += 5000000;
            balances[ownerTeam] += 500000;
            balances[ASI] += 50000;
            balances[owner] -= 5550000;
        }
        emit TeamReg(_users);
    }
    
}
