import telebot
from telebot import types
from web3 import Web3, HTTPProvider
from web3.utils.transactions import wait_for_transaction_receipt
import eth_utils
import json

k1 = types.ReplyKeyboardMarkup()
k1.row(types.KeyboardButton(text='Создать'))  # generate address in ethereum
k1.row(types.KeyboardButton(text='Добавить'))  # import existing address in ethereum
k1.row(types.KeyboardButton(text='Баланс'))
k1.row(types.KeyboardButton(text='Отправить'))
k1.row(types.KeyboardButton(text='Получить токены'))

k2 = types.ReplyKeyboardMarkup()
k2.row(types.KeyboardButton(text="Отправить номер", request_contact=True))

k3 = types.ReplyKeyboardMarkup()
k3.row(types.KeyboardButton(text="Я сохранил"))

k4 = types.ReplyKeyboardMarkup()
k4.row(types.KeyboardButton(text="По кошельку"))
k4.row(types.KeyboardButton(text="По нику Telegram"))
k4.row(types.KeyboardButton(text="По номеру телефона"))
k4.row(types.KeyboardButton(text="Отмена"))

k5 = types.ReplyKeyboardMarkup()
k5.row(types.KeyboardButton(text="Отмена"))

api = "628177061:AAHoxMOzA6SKZ3wQ1PxhD63zZYAnNnT-G7Y"

password = "Unicorn1021"

bot = telebot.TeleBot(api)

pk = "47fb62d06684d23e676afaa912293f47294cb79c0276bef14d5267a7849ddceb"

contract_address = "0x521Ec5853B48CA7Af52542453202692831839096"
abiContr = """[{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,
"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"",
"type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{
"name":"_user","type":"address"}],"name":"balanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,
"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_user1","type":"address"},
{"name":"_user2","type":"address"},{"name":"_user3","type":"address"},{"name":"_user4","type":"address"},
{"name":"_user5","type":"address"},{"name":"_user6","type":"address"}],"name":"teamReg","outputs":[],"payable":false,
"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"",
"type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{
"name":"_to","type":"address"},{"name":"_amount","type":"uint256"}],"name":"transfer","outputs":[],"payable":false,
"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"myBalance","outputs":[{
"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,
"inputs":[{"name":"_user","type":"address"}],"name":"personalReg","outputs":[],"payable":false,
"stateMutability":"nonpayable","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable",
"type":"constructor"},{"payable":true,"stateMutability":"payable","type":"fallback"},{"anonymous":false,"inputs":[{
"indexed":false,"name":"_team","type":"address[]"}],"name":"TeamReg","type":"event"},{"anonymous":false,"inputs":[{
"indexed":false,"name":"_from","type":"address"},{"indexed":false,"name":"_to","type":"address"},{"indexed":false,
"name":"_amount","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,
"name":"_user","type":"address"}],"name":"PersonalReg","type":"event"}] """

kovan = Web3(HTTPProvider("https://kovan.infura.io/"))

backup = open("backup.txt")
isAdmin = json.loads(backup.readline())
data = json.loads(backup.readline())
pks = json.loads(backup.readline())
numbers = json.loads(backup.readline())
nicks = json.loads(backup.readline())
backup.close()
addresses = {}

for i in pks.keys():
    addresses[int(i)] = kovan.eth.account.privateKeyToAccount(pks[i])

contract = kovan.eth.contract(address=kovan.toChecksumAddress(contract_address), abi=abiContr)
account = kovan.eth.account.privateKeyToAccount(pk)


def update():
    backup = open("backup.txt", "w")
    backup.write(json.dumps(isAdmin) + "\n")
    backup.write(json.dumps(data) + "\n")
    backup.write(json.dumps(pks) + "\n")
    backup.write(json.dumps(numbers) + "\n")
    backup.write(json.dumps(nicks))
    backup.close()


def importAcc(m):  # import existing address
    a = (m.text.startswith("0x") and len(m.text) == 66) or len(m.text) == 64
    if a:  # successfully
        try:
            int(m.text, 16)
            pks[int(m.chat.id)] = a
            addresses[m.chat.id] = kovan.eth.account.privateKeyToAccount(m.text)
            if m.chat.id not in numbers.values():
                bot.send_message(m.chat.id, "Отправьте ваш номер телефона по кнопке ниже, чтобы продолжить",
                                 reply_markup=k2)
                data[m.chat.id] = "contact2"
            else:
                bot.send_message(m.chat.id, "Успешно импортировано", reply_markup=k1)
        except:
            bot.send_message(m.chat.id, "Неверные данные, повторите попытку")
    else:  # unsuccessfully
        bot.send_message(m.chat.id, "Неверные данные, повторите попытку")


def sendTx(addr):  # send UNC tokens
    data = contract.encodeABI('teamReg', tuple(addr))

    nonce = kovan.eth.getTransactionCount(account.address)

    tx = {'to': contract.address,
          'gas': 1000000,
          'gasPrice': Web3.toWei(0.01, 'gwei'),
          'chainId': 42,
          'value': 0,
          'data': data,
          'nonce': nonce
          }

    signed = account.signTransaction(tx)

    tx = kovan.eth.sendRawTransaction(signed.rawTransaction)

    wait_for_transaction_receipt(kovan, tx)
    rec = kovan.eth.getTransactionReceipt(tx)
    return (rec)


def getBalance(addr):  # get the balance
    rec = contract.functions.balanceOf(addr).call()
    return (rec)


def receiveTokens(addr, m):
    a = kovan.isAddress(kovan.toChecksumAddress(addr))
    if a:  # if input is an address
        bot.send_message(m.chat.id, "Подключение к блокчейн-сети...")
        data = contract.encodeABI('personalReg', addr.split())
        nonce = kovan.eth.getTransactionCount(account.address)
        tx = {'to': contract.address,
              'gas': 1000000,
              'gasPrice': Web3.toWei(0.01, 'gwei'),
              'chainId': 42,
              'value': 0,
              'data': data,
              'nonce': nonce}
        signed = account.signTransaction(tx)
        tx = kovan.eth.sendRawTransaction(signed.rawTransaction)
        wait_for_transaction_receipt(kovan, tx)
        rec = kovan.eth.getTransactionReceipt(tx)
        if rec["status"] == 0:  # if user already got 1000000 UNC
            bot.send_message(m.chat.id, "Пользователь уже получал свой миллион")
        else:  # if all is succesful
            nonce = kovan.eth.getTransactionCount(account.address)
            tx2 = {'to': addr,
                   'gas': 1000000,
                   'gasPrice': Web3.toWei(0.01, 'gwei'),
                   'chainId': 42,
                   'value': Web3.toWei(0.000001, "ether"),
                   'nonce': nonce}

            signed = account.signTransaction(tx2)
            tx2 = kovan.eth.sendRawTransaction(signed.rawTransaction)
            wait_for_transaction_receipt(kovan, tx2)
            bot.send_message(m.chat.id, "Успешно перечислено")
    else:  # sending the error message
        bot.send_message(m.chat.id, "Ошибка, проверьте введенные данные")


def transferTokens(fromt, to, count):
    data = contract.encodeABI('transfer', (to, int(count)))

    nonce = kovan.eth.getTransactionCount(fromt.address)

    tx = {'to': contract.address,
          'gas': 1000000,
          'gasPrice': 1,
          'chainId': 42,
          'value': 0,
          'data': data,
          'nonce': nonce
          }

    signed = fromt.signTransaction(tx)

    tx = kovan.eth.sendRawTransaction(signed.rawTransaction)

    wait_for_transaction_receipt(kovan, tx)


def send(m):
    try:
        rec = sendTx(m.text.split())
        if rec["status"] == 0:  # if something get wrong
            bot.send_message(m.chat.id, "Ошибка, проверьте введенные данные")
        else:  # if all is succesful
            bot.send_message(m.chat.id, "Успешно перечислено")
    except:  # if there was an error on the server side
        bot.send_message(m.chat.id, "Ошибка, проверьте введенные данные")


@bot.message_handler(commands=['start'])
def start(m):  # first inviting message
    # describing possible functions for a common user
    bot.send_message(m.chat.id,
                     "*Создать* - создать новый кошелек\n*Добавить* - добавить кошелек по приватному ключу\n*Баланс* "
                     "- узнать текущий баланс UNC\n*Отправить* - отправить токены по кошельку, номеру телефона или "
                     "никнейму\n*Получить токены* - получить 1 000 000 UNC и 0.000001 ETH",
                     reply_markup=k1, parse_mode='Markdown')


@bot.message_handler(commands=['send'])
def sender(m):
    if m.chat.id in isAdmin:
        for i in pks.keys():
            bot.send_message(i, m.text[5:], reply_markup=k1)
    else:
        bot.send_message(m.chat.id, "Неизвестная команда", reply_markup=k1, parse_mode='Markdown')


@bot.message_handler(content_types=['contact'])
def cont(m):
    if m.chat.id in data.keys():
        if m.chat.id == m.contact.user_id:
            numbers[m.contact.phone_number] = m.chat.id
            if data[m.chat.id] == "contact2":
                nicks[m.from_user.username] = m.chat.id
                data.pop(m.chat.id)
                bot.send_message(m.chat.id, "Успешно импортировано", reply_markup=k1)
                update()
            else:
                pklo = eth_utils.to_hex(eth_utils.crypto.keccak_256(bytes(m.chat.id)))
                bot.send_message(m.chat.id,
                                 "Сохраните приватный ключ, после сохранения он удалится без возможности восстановления")
                data[m.chat.id] = bot.send_message(m.chat.id, pklo, reply_markup=k3).message_id
                addresses[m.chat.id] = kovan.eth.account.privateKeyToAccount(pklo)  # adding address to the local dict
                pks[int(m.chat.id)] = pklo
        else:
            bot.send_message(m.chat.id, "Это не ваш контакт, повторите попытку")
    else:
        bot.send_message(m.chat.id, "Неизвестная команда")


def workingWithData(m):
    if m.text == "Отмена":
        bot.send_message(m.chat.id, "Успешно отменено", reply_markup=k1)
        data.pop(m.chat.id)
    elif data[m.chat.id] == "import":
        importAcc(m)
    elif data[m.chat.id] == "send":
        if m.text == "По кошельку":
            bot.send_message(m.chat.id, "Введите кошелек", reply_markup=k5)
            data[m.chat.id] = "wallet"
        elif m.text == "По нику Telegram":
            bot.send_message(m.chat.id, "Введите ник в формате @Chain_the_Bot", reply_markup=k5)
            data[m.chat.id] = "nick"
        elif m.text == "По номеру телефона":
            bot.send_message(m.chat.id, "Введите номер телефона в формате +79991112244", reply_markup=k5)
            data[m.chat.id] = "number"
        else:
            bot.send_message(m.chat.id, "Выберите способ отправки", reply_markup=k4)
    elif data[m.chat.id] == "wallet":
        a = kovan.isAddress(kovan.toChecksumAddress(m.text))
        if a:
            bot.send_message(m.chat.id, "Введите количество токенов", reply_markup=k5)
            data[m.chat.id] = (m.text, 0)
        else:
            bot.send_message(m.chat.id, "Неправильные данные, повторите попытку", reply_markup=k5)
    elif data[m.chat.id] == "nick":
        if m.text[1:] in nicks.keys():
            bot.send_message(m.chat.id, "Введите количество токенов", reply_markup=k5)
            data[m.chat.id] = (addresses[nicks[m.text[1:]]].address, nicks[m.text[1:]])
        else:
            bot.send_message(m.chat.id,
                             "Данный пользователь не использует нашего бота, проверьте данные и повторите попытку",
                             reply_markup=k5)
    elif data[m.chat.id] == "number":
        if m.text[1:] in numbers.keys():
            bot.send_message(m.chat.id, "Введите количество токенов", reply_markup=k5)
            data[m.chat.id] = (addresses[numbers[m.text[1:]]].address, numbers[m.text[1:]])
        else:
            bot.send_message(m.chat.id, "Данный номер не привязан к нашему боту, проверьте данные и повторите попытку",
                             reply_markup=k5)
    elif type(data[m.chat.id]) == tuple:
        if data[m.chat.id][0].startswith("0x"):
            if m.text.isdigit() and m.text != "0":
                if int(m.text) <= getBalance(addresses[m.chat.id].address):
                    bot.send_message(m.chat.id, "Подключение к блокчейн-сети...")
                    transferTokens(addresses[m.chat.id], data[m.chat.id][0], m.text)
                    bot.send_message(m.chat.id, "Успешно перечислено", reply_markup=k1)
                    bot.send_message(data[m.chat.id][1],
                                     "@" + m.from_user.username + " отправил вам " + m.text + " UNC",
                                     reply_markup=k1)
                    data.pop(m.chat.id)
                else:
                    bot.send_message(m.chat.id, "Вам не хватает " + str(
                        int(m.text) - getBalance(addresses[m.chat.id].address)) + " UNC", reply_markup=k1)
                    data.pop(m.chat.id)
            else:
                bot.send_message(m.chat.id, "Введите целое положительное число")


# if user sent something
@bot.message_handler(content_types=['text'])
def reg1(m):
    if (m.chat.id in data.keys()) and (m.text != "Я сохранил"):
        workingWithData(m)
    elif m.text == password:
        bot.send_message(m.chat.id, "Вы в режиме администрирования")
        isAdmin[m.chat.id] = True
        update()
    elif (m.text[0] == "0") and (m.chat.id in isAdmin.keys()):  # if it is address for sending UNC
        bot.send_message(m.chat.id, "Подключение к блокчейн-сети...")
        send(m)
    elif m.text == "Создать":
        if m.chat.id not in addresses.keys():
            bot.send_message(m.chat.id, "Отправьте ваш номер телефона по кнопке ниже, чтобы продолжить",
                             reply_markup=k2)
            data[m.chat.id] = "contact"
        else:
            bot.send_message(m.chat.id, "У вас уже есть адрес:")
            bot.send_message(m.chat.id, addresses[m.chat.id].address)
    elif (m.text == "Я сохранил") and (m.chat.id in data):
        if data[m.chat.id] != "contact":
            bot.delete_message(chat_id=m.chat.id, message_id=data[m.chat.id])
            data.pop(m.chat.id)
            nicks[m.from_user.username] = m.chat.id
            bot.send_message(m.chat.id, "Ваш адрес:")
            bot.send_message(m.chat.id, addresses[m.chat.id].address, reply_markup=k1)
            update()
        else:
            bot.send_message(m.chat.id, "Вам надо отправить контакт")
    elif m.text == "Баланс":  # getting the balance for local account
        if m.chat.id in addresses.keys():
            s = getBalance(addresses[m.chat.id].address)
            t = Web3.fromWei(kovan.eth.getBalance(addresses[m.chat.id].address), "ether")
            bot.send_message(m.chat.id,
                             "*Баланс*:\n" + str(("%.18f" % t).rstrip('0').rstrip('.')) + " ETH\n" + str(s) + " UNC",
                             parse_mode='Markdown')  # sending message with balance
        else:  # if there is no local address
            bot.send_message(m.chat.id, "Нет кошелька в боте")
    elif m.text == "Добавить":  # import the address
        bot.send_message(m.chat.id, "Введите приватный ключ от своего кошелька", reply_markup=k5)
        data[m.chat.id] = "import"  # flag for importing
    elif m.text == "Получить токены":
        if m.chat.id in addresses.keys():
            receiveTokens(addresses[m.chat.id].address, m)
        else:
            bot.send_message(m.chat.id, "Нет кошелька в боте")
    elif m.text == "Отправить":
        if m.chat.id in addresses.keys():
            bot.send_message(m.chat.id, "Выберите способ отправки", reply_markup=k4)
            data[m.chat.id] = "send"
        else:
            bot.send_message(m.chat.id, "Нет кошелька в боте")
    else:  # if user sent unknown command
        bot.send_message(m.chat.id, "Неизвестная команда")


bot.infinity_polling()